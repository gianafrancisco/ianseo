#!/bin/bash

USER=$USERNAME
PASS=$PASSWORD

if [ -z $USER ]
then
    USER=ianseo
fi

if [ ! -f /opt/.init_password ]
then
    if [ -z $PASS ]
    then
        PASS=`date | md5sum | cut -c -10`
    fi
    echo "User: ${USER}, Password: ${PASS}" > /opt/.init_password
    htpasswd -bc /opt/.ianseo_htpasswd $USER $PASS 
fi
echo "Credential access"
cat  /opt/.init_password
echo "System started"

apachectl -D FOREGROUND