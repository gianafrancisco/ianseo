variable "region" {
  default = "us-east-2"
}

variable "app" {
  default = "Ianseo"
}

variable "vpc_id" {
  default = "vpc-1b66a172"
}

variable "subnet_id" {
  default = "subnet-e8ad7e81"
}

variable "private_key" {
  type = string
  default = "~/git/francisco.pem"
}

variable "key_name" {
  type = string
  default = "francisco"
}

variable "dns_zone" {
  type = string
  default = "gianafrancisco.com.ar."
}


variable "tags" {
  type = map(string)
  default = {
    "Project"       = "Piamontesa - Ianseo"
    "Environment"   = "Development"
    "Email"         = "gianafrancisco@gmail.com"
  }
}