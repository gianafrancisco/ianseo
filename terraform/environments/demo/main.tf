

data "aws_subnet_ids" "subnets" {
  vpc_id = var.vpc_id
}

module "ianseo" {
  source      = "../../modules/ec2"
  key_name    = var.key_name
  subnet_id   = var.subnet_id
  private_key = var.private_key
  tags        = var.tags
}

# module "dns" {
#   source      = "../../modules/dns"
#   dns_record  = "ianseo"
#   dns_name    = module.ianseo.dns_name
#   tags        = var.tags
#   zone        = var.dns_zone
# }
