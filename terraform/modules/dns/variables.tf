variable "dns_record" {
  type = string
}

variable "dns_name" {
  type = string
}

variable "zone" {
  type = string
}

variable "tags" {
    type = map(string)
}