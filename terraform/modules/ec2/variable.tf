variable "type" {
  default = "t2.micro"
}

variable "ami" {
  default = "ami-064ff912f78e3e561"
}

variable "subnet_id" {
}

variable "key_name" {
  default = "ianseo"
}

variable "vpc_id" {
  default = "vpc-1b66a172"
}

variable "tags" {
  type = map(string)
  default = {}
}

variable "private_key" {
  type = string
}