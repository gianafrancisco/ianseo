resource "aws_instance" "instance" {
  ami           = var.ami
  instance_type = var.type
  associate_public_ip_address = true
  subnet_id = var.subnet_id
  tags = var.tags
  key_name = var.key_name
  vpc_security_group_ids = [aws_security_group.allow_traffic.id]

  provisioner "remote-exec" {
      inline = ["sudo yum update -y; echo Done!"]

      connection {
        host        = self.public_dns
        type        = "ssh"
        user        = "ec2-user"
        private_key = file(var.private_key)
      }
    }

    provisioner "local-exec" {
      command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ec2-user -i '${self.public_dns},' --private-key ${var.private_key} ${path.module}/../../../ansible/ianseo.yaml"
    }

}


resource "aws_security_group" "allow_traffic" {
  name        = "allow_traffic"
  description = "Allow inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description      = "ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }


  ingress {
    description      = "http"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "https"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = var.tags
}