# Ianseo

## Description

This repo was created to deploy Ianseo in a cloud or locally using docker and docker-compose

## Dependencies

* docker
* docker-compose
* Terraform
* Ansible (pip install ansible)
* make

## Deployment

### Terraform and AWS

To start a EC2 instance on aws and deploy Ianseo, you should run next steps:

* Set AWS environment variables

```bash
export AWS_ACCESS_KEY_ID=<yours aws access key id>
export AWS_SECRET_ACCESS_KEY=<yours aws secret access key>
export AWS_DEFAULT_REGION=us-east-2
```

* go to demo directory

```bash
cd terraform/environments/demo/
terraform init
```

* Check plan and deploy

```bash
terraform plan # Check that there are not errors
terraform apply # When asked to deploy say 'yes'
```

* In the output logs you will get the url to connect to Ianseo as well the default password (the password set by default is the same configured in docker-compose.yml)

![Default Password](docs/ansible-output.png)

### Deploy locally using docker compose

To run it locally edit `docker-compode.yml` to point to the latest docker image or local one and the run

```bash
docker-compode up -d
# or docker compose up -d
# alternative you can use `make run` as well
```

## Build

To build a local docker image you should run

```bash
make build
```
