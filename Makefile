docker_io_image = "ianseo:latest"
branch = "main"
name = "ianseo"
tag = "20220701"
tests = "tests"
artifact = "https://www.ianseo.net/Release/Ianseo_20220701.zip"
DOCKER_USER = "fgiana"

build: 
	docker rmi ${name}:${tag} || echo ""
	docker build -f docker/Dockerfile \
	--build-arg IANSEO=${artifact} \
	-t ${name}:${tag} docker

push: build tag
	docker login -u ${DOCKER_USER}
	docker push ${DOCKER_USER}/${name}:${tag}

tag: 
	# DOCKER_USER=`docker info | grep "Username" | awk -F: '{print $NF}'`
	docker tag ${name}:${tag} ${DOCKER_USER}/${name}:${tag}

run:
	docker-compose up -d

stop:
	docker-compose down

clean:
	docker rmi ${name}:${tag} || true
	docker login -u ${DOCKER_USER}
	# DOCKER_USER=$(docker info | grep "Username" | awk -F: '{print $NF}')
	docker rmi ${DOCKER_USER}/${name}:${tag} || true
